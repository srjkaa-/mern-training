const express = require('express');
const router = express.Router();
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');

// Load UserModel
const User = require('../../models/UserModel');

// @route   get api/users/test
// @desc    Tests users route
// @access  Private
router.get('/test', (req, res) => res.json({msg: 'Users works!'}));

// @route   get api/users/register
// @desc    Register user
// @access  Public
router.post('/register', (req, res) => {
  User.findOne({ email: req.body.email })
    .then(user => {

      if (user) {
        return res.status(400).json({ email: 'Email already exists' });
      } else {

        const email = req.body.email;
        const avatar = gravatar.url(email, {
          s: 200, // Size
          r: 'pg', // Raing,
          d: 'mm' // Default
        });

        const newUser = User({
          name: req.body.name,
          email: email,
          avatar,
          password: req.body.password
        });

        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw new Error(err);
            newUser.password = hash;
            newUser
              .save()
              .then((user => res.json(user)))
              .catch((err) => console.log(err));
          })
        });

      }

    });
});

module.exports = router;